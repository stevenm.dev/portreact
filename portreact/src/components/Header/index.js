import React from 'react';

const Header = () => {
    return(
        <header>
            <div className="banner-container">
                <h1><a href="/">PortReact</a></h1>
            
            </div>
        </header>
        
    )
}

export default Header;