import React, { useRef, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useAuth } from '../context/AuthContext'


const Signup = (props) => {

    const pseudoRef = useRef()
    const emailRef = useRef()
    const passwordRef = useRef()
    const confirmPasswordRef = useRef()
    const { signUp } = useAuth()
    const [error, setError] = useState('')
    const [loading, setLoading] = useState(false)
    const history = useHistory()


    // Submit form with async await function()
    async function handleSubmit(e) {
        e.preventDefault()

        // Check if password === confirm password else display error
        if(passwordRef.current.value !== confirmPasswordRef.current.value) {
            // console.log(passwordRef.current.value)
            // console.log(confirmPasswordRef.current.value)
            return setError('Password do not match')
        }

        try {
            setError('')
            setLoading(true)
            await signUp(emailRef.current.value, passwordRef.current.value)
            history.push("/welcome")
        } catch {
            setError('Failed to create an account')
        }
        setLoading(false)    
    }

    // Gestion error

    const errorMsg = error !== '' && <span>{error}</span>;

    return (
        <div className="signUpLoginBox">
            <div className="slContainer">
                <div className="formBoxLeftSignup">

                </div>
                <div className="formBoxRight">
                    <div className="formContent">
                        
                        {errorMsg}
                        <h2>Incription</h2>
                        <form onSubmit={handleSubmit}>
                                                
                            <div className="inputBox">
                                <input  ref={pseudoRef} type="text" id="pseudo" autoComplete="off" required />
                                <label htmlFor="pseudo">Pseudo</label>
                            </div>

                            <div className="inputBox">
                                <input  ref={emailRef} type="text" id="email" autoComplete="off" required />
                                <label htmlFor="email">Email</label>
                            </div>

                            <div className="inputBox">
                                <input  ref={passwordRef} type="password" id="password" autoComplete="off" required />
                                <label htmlFor="password">Mot de passe</label>
                            </div>

                            <div className="inputBox">
                                <input  ref={confirmPasswordRef} type="password" id="confirmPassword" autoComplete="off" required />
                                <label htmlFor="confirmPassword">Confirmer le mot de passe</label>
                            </div>

                            <button disabled={loading}>Inscription</button>
                        </form>
                        <div className="linkContainer">
                            <Link className="simpleLink" to="/login">Déjà inscrit ? Connectez-vous.</Link>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}

export default Signup;
