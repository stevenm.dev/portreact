import React, { useContext, useState, useEffect } from 'react'
import { auth } from '../firebase/firebase'

const AuthContext = React.createContext()

export function useAuth() {
    return useContext(AuthContext)
}

function AuthProvider({children}) {

    const [currentUser, setCurrentUser] = useState()
    const [loading, setLoading] = useState(true)

    // SignUp function()
    function signUp(email, password) {
        return auth.createUserWithEmailAndPassword(email, password)
    }

    // Login function()
    function login(email, password) {
        return auth.signInWithEmailAndPassword(email, password)
    }

    // Logout function()
    function logout() {
        return auth.signOut()
    }

    function resetPassword(email) {
        return auth.sendPasswordResetEmail(email)
    }

    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged(user => {
            setCurrentUser(user)
            setLoading(false)   
        })

        return unsubscribe
    }, [])
    
    // Put all function in value so we can use it with 
    // import { useAuth } from '../context/AuthContext'
    const value = {
        currentUser,
        login,
        signUp,
        logout,
        resetPassword
    }

    return (
        <AuthContext.Provider value={value}>
            {!loading && children}
        </AuthContext.Provider>
    )
}


export default AuthProvider;