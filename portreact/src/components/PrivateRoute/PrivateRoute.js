import React, {Fragment} from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';

export default function PrivateRoute({component: Component, ...rest}) {

    const { currentUser } = useAuth()

    return (
        <Route
        {...rest}
        render={props => {
           return currentUser ? <Component {...props} /> : <Redirect to="/login" />
            }}
        >
            
        </Route>
    )
}


// mettre en place un loader avant de renvoyer sur la page de login
// ( <Fragment>
//     <div className="loader">
//     {setTimeout(() =>{
//      <Redirect to="/login" />
//     }, 1000)}
//     </div>
    
// </Fragment>)