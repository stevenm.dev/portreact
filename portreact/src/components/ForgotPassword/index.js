
import React, { useState, useRef } from 'react'
import { Link } from 'react-router-dom';
import { useAuth } from '../context/AuthContext'


function ForgotPassword(props) {

    const emailRef = useRef()
    const { resetPassword } = useAuth()
    const [error, setError] = useState('')
    const [loading, setLoading] = useState(false)
    const [message, setMessage] = useState('')

    // Submit form with async await function()
    async function handleSubmit(e) {
        e.preventDefault()

        try {
            setMessage('')
            setError('')
            setLoading(true)
            await resetPassword(emailRef.current.value) // Check if email exist
            setMessage("Regardez vos mails pour plus d'instructions")
        } catch {
            setError('Failed to reset password')
        }
        setLoading(false)    
    }

    return (
        <div className="signUpLoginBox">
            <div className="slContainer">
                <div className="formBoxLeftLogin">

                </div>
                <div className="formBoxRight">
                    <div className="formContent">
                        {error !== '' && <span>{error}</span>}
                        {message && <span className="alert alert-success">{message}</span>}
                        
                        <h2>Réinitialiser Mot de passe</h2>
                        <form onSubmit={handleSubmit}> 

                        <div className="inputBox">
                                <input  ref={emailRef} type="text" id="email" autoComplete="off" required />
                                <label htmlFor="email">Email</label>
                            </div>

                            <button disabled={loading}>Réinitialiser le mot de passe</button>       

                            <div className="">
                            <Link className="simpleLink" to="/login">Déjà inscrit ? Connectez-vous.</Link>
                        </div>             
                            
                        </form>
                        <div className="linkContainer">
                            <Link className="simpleLink" to="/signup">Nouveau ? Inscrivez-vous.</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ForgotPassword;
