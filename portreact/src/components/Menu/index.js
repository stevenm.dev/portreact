import React from 'react'
import { NavLink } from 'react-router-dom';

function Menu() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            
            <NavLink className="navbar-brand" to="/">Home</NavLink>
            
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav ms-auto">
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/Welcome">Welcome</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/fusion">Fusion</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/community">Community</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/users/:profileId">Users</NavLink>
                    </li>                    
                </ul>
            </div>            
        </nav>
    )
}

export default Menu;
