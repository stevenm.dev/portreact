import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import ErrorPage from '../ErrorPage';
import Header from '../Header';
import Menu from '../Menu';
import Footer from '../Footer';
import Landing from '../Landing';
import Welcome from '../Welcome';
import Fusion from '../FusionChart'
import Login from '../Login';
import Signup from '../Signup';
import AuthProvider from "../context/AuthContext";
import PrivateRoute from '../PrivateRoute/PrivateRoute';
import ForgotPassword from "../ForgotPassword"


import '../../App.css';


function App() {
  return (
    <Router>
      <AuthProvider>
        <Header />
        <Menu />

        <Switch>
          <Route exact path="/" component={Landing} />
          <PrivateRoute path="/welcome" component={Welcome} />
          <PrivateRoute path="/fusion" component={Fusion} />
          <Route path="/login" component={Login} />
          <Route path="/signup" component={Signup} />
          <Route path="/forgotpassword" component={ForgotPassword} />
          <Route component={ErrorPage} />
        </Switch>

        <Footer />
      </AuthProvider>
    </Router>
  );
}

export default App;
