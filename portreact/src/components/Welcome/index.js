import React, { useState, Fragment } from 'react';
import { useAuth } from '../context/AuthContext';
import { useHistory } from 'react-router-dom';


const Welcome = (props) => {

    const [error, setError] = useState("")
    const { logout } = useAuth()
    const history = useHistory()

    async function handleLogout() {
        setError('')

        try {
            await logout()
            history.push('/login')
        } catch {
            setError('Failed ot log out')
        }
    }

    return  (
    <Fragment>
        <h2 style={{color: 'white'}} className="text-center">Vous êtes bien connectés</h2>
            

            <div className="logout">
            <button onClick={handleLogout}>Déconnexion</button>  
            </div>
            

        </Fragment>
    )
}

export default Welcome;
