import React, { useState, useRef } from 'react'
import { Link, useHistory } from 'react-router-dom';
import { useAuth } from '../context/AuthContext'


const Login = (props) => {

    const emailRef = useRef()
    const passwordRef = useRef()
    const { login } = useAuth()
    const [error, setError] = useState('')
    const [loading, setLoading] = useState(false)
    const history = useHistory()

    // Submit form with async await function()
    async function handleSubmit(e) {
        e.preventDefault()

        try {
            setError('')
            setLoading(true)
            await login(emailRef.current.value, passwordRef.current.value) // Check if email and password check and exist
            history.push("/welcome")
        } catch {
            setError('Failed to sign in')
        }
        setLoading(false)    
    }

    return (
        <div className="signUpLoginBox">
            <div className="slContainer">
                <div className="formBoxLeftLogin">

                </div>
                <div className="formBoxRight">
                    <div className="formContent">
                        {error !== '' && <span>{error}</span>}
                        
                        <h2>Connexion</h2>
                        <form onSubmit={handleSubmit}> 

                        <div className="inputBox">
                                <input  ref={emailRef} type="text" id="email" autoComplete="off" required />
                                <label htmlFor="email">Email</label>
                            </div>

                            <div className="inputBox">
                                <input  ref={passwordRef} type="password" id="password" autoComplete="off" required />
                                <label htmlFor="password">Mot de passe</label>
                            </div> 

                            <button disabled={loading}>Connexion</button>       

                            <div className="">
                            <Link className="simpleLink" to="/forgotpassword">Mot de passe oublié ?</Link>
                        </div>             
                            
                        </form>
                        <div className="linkContainer">
                            <Link className="simpleLink" to="/signup">Nouveau ? Inscrivez-vous.</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;
